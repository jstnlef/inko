import std::iterator::(self, Enumerator, Iterator)
import std::test
import std::test::assert

object EmptyIterator {}

impl Iterator!(Integer) for EmptyIterator {
  def next? -> Boolean {
    False
  }

  def next -> ?Integer {
    Nil
  }
}

object SimpleIterator {
  @values: Array!(Integer)
  @index: Integer

  def init {
    @values = Array.new(10, 20, 30)
    @index = 0
  }
}

impl Iterator!(Integer) for SimpleIterator {
  def next -> ?Integer {
    let value = @values[@index]

    @index += 1

    value
  }

  def next? -> Boolean {
    @index < @values.length
  }
}

def integer_enumerator -> Iterator!(Integer) {
  let array = Array.new(10, 20, 30)
  let mut index = 0

  Enumerator.new(
    while: { index < array.length },
    yield: {
      let value = array[index]

      index += 1

      value
    }
  )
}

test.group('std::iterator::Iterator.each') do (g) {
  g.test('Iterating over the values in an Iterator') {
    let iter = SimpleIterator.new
    let values = Array.new

    let returned = iter.each do (value) {
      values.push(value)
    }

    assert.equal(returned, iter)
    assert.equal(values, Array.new(10, 20, 30))
  }

  g.test('Iterating over the values in an empty Iterator') {
    let iter = EmptyIterator.new
    let values = Array.new

    iter.each do (value) {
      values.push(value)
    }

    assert.equal(values, Array.new)
  }
}

test.group('std::iterator::Iterator.find') do (g) {
  g.test('Finding the first occurrence of a value in an Iterator') {
    let iter = SimpleIterator.new
    let val = iter.find do (value) { value == 20 }

    assert.equal(val, 20)
  }

  g.test('Finding the first occurrence of a value in an empty Iterator') {
    let iter = EmptyIterator.new
    let val = iter.find do (value) { value == 20 }

    assert.equal(val, Nil)
  }
}

test.group('std::iterator::Iterator.any?') do (g) {
  g.test('Checking if an Iterator includes a value') {
    let iter = SimpleIterator.new

    assert.true(iter.any? do (value) { value == 20 })
    assert.false(iter.any? do (value) { value == 200 })
  }
}

test.group('std::iterator::Iterator.map') do (g) {
  g.test('Mapping the values of an Iterator to a different value') {
    let iter = SimpleIterator.new.map do (val) { val * 2 }
    let values = Array.new

    iter.each do (value) {
      values.push(value)
    }

    assert.equal(values, Array.new(20, 40, 60))
  }
}

test.group('std::iterator::Iterator.select') do (g) {
  g.test('Selecting values from an empy Iterator') {
    let iter = EmptyIterator.new
    let vals = iter.select do (value) { value > 10 }.to_array

    assert.true(vals.empty?)
  }

  g.test('Selecting values from an Iterator with values') {
    let iter = SimpleIterator.new
    let vals = iter.select do (value) { value > 10 }.to_array

    assert.equal(vals, Array.new(20, 30))
  }

  g.test('Selecting values from an Iterator when no values match') {
    let iter = SimpleIterator.new
    let vals = iter.select do (value) { value > 50 }.to_array

    assert.true(vals.empty?)
  }
}

test.group('std::iterator::Iterator.partition') do (g) {
  g.test('Partitioning an empty Iterator') {
    let iter = EmptyIterator.new
    let pair = iter.partition do (value) { value >= 20 }

    assert.true(pair.first.empty?)
    assert.true(pair.second.empty?)
  }

  g.test('Partitioning an Iterator with value') {
    let iter = SimpleIterator.new
    let pair = iter.partition do (value) { value >= 20 }

    assert.equal(pair.first, Array.new(20, 30))
    assert.equal(pair.second, Array.new(10))
  }
}

test.group('std::iterator::Iterator.to_array') do (g) {
  g.test('Converting an Iterator to an Array') {
    let iter = SimpleIterator.new
    let vals = iter.to_array

    assert.equal(vals, Array.new(10, 20, 30))
  }
}

test.group('std::iterator::Enumerator.next') do (g) {
  g.test('Obtaining the next value in an Enumerator') {
    let enum = integer_enumerator

    assert.equal(enum.next, 10)
    assert.equal(enum.next, 20)
    assert.equal(enum.next, 30)
    assert.equal(enum.next, Nil)
  }
}

test.group('std::iterator::Enumerator.next?') do (g) {
  g.test('Checking if there are remaining values in an Enumerator') {
    let enum = integer_enumerator

    assert.true(enum.next?)
    enum.next

    assert.true(enum.next?)
    enum.next

    assert.true(enum.next?)
    enum.next

    assert.false(enum.next?)
  }
}

test.group('std::iterator.index_enumerator') do (g) {
  g.test('Iterating over an Array') {
    let numbers = Array.new(10, 20, 30)
    let enum: Enumerator!(Integer) =
      iterator.index_enumerator(numbers.length) do (index) {
        numbers[index]
      }

    assert.equal(enum.next, 10)
    assert.equal(enum.next, 20)
    assert.equal(enum.next, 30)
    assert.equal(enum.next, Nil)
  }
}

test.group('std::iterator.reverse_index_enumerator') do (g) {
  g.test('Iterating over an Array in reverse order') {
    let numbers = Array.new(10, 20, 30)
    let enum: Enumerator!(Integer) =
      iterator.reverse_index_enumerator(numbers.length) do (index) {
        numbers[index]
      }

    assert.equal(enum.next, 30)
    assert.equal(enum.next, 20)
    assert.equal(enum.next, 10)
    assert.equal(enum.next, Nil)
  }
}
