import std::test
import std::test::assert

test.group('std::array::Array.new') do (g) {
  g.test('Creating an empty Array') {
    assert.equal(Array.new.length, 0)
  }

  g.test('Creating an Array with one value') {
    assert.equal(Array.new(10).length, 1)
  }

  g.test('Creating an Array with multiple values') {
    assert.equal(Array.new(10, 20, 30).length, 3)
  }
}

test.group('std::array::Array.clear') do (g) {
  g.test('Removing all values from an Array') {
    let numbers = Array.new(10, 20, 30)

    assert.equal(numbers.clear, Array.new)
    assert.equal(numbers, Array.new)
  }
}

test.group('std::array::Array.push') do (g) {
  g.test('Adding a value to the end of an Array') {
    let numbers = Array.new

    numbers.push(10)
    numbers.push(20)

    assert.equal(numbers, Array.new(10, 20))
  }
}

test.group('std::array::Array.pop') do (g) {
  g.test('Removing a value from the end of an Array') {
    let numbers = Array.new(10, 20)

    assert.equal(numbers.pop, 20)
    assert.equal(numbers, Array.new(10))
  }
}

test.group('std::array::Array.remove_at') do (g) {
  g.test('Removing a value using an existing index') {
    let numbers = Array.new(10, 20)

    assert.equal(numbers.remove_at(0), 10)
    assert.equal(numbers, Array.new(20))
  }

  g.test('Removing a value using a non-existing index') {
    let numbers = Array.new(10)

    assert.equal(numbers.remove_at(1), Nil)
    assert.equal(numbers, Array.new(10))
  }
}

test.group('std::array::Array.each') do (g) {
  g.test('Iterating over the values of an Array') {
    let input = Array.new(10, 20, 30)
    let output = Array.new
    let returned = input.each do (number) {
      output.push(number)
    }

    assert.equal(returned, input)
    assert.equal(output, Array.new(10, 20, 30))
  }
}

test.group('std::array::Array.each_with_index') do (g) {
  g.test('Iterating over the values and indexes of an Array') {
    let input = Array.new(10, 20, 30)
    let output = Array.new
    let indexes = Array.new

    let returned = input.each_with_index do (number, index) {
      output.push(number)
      indexes.push(index)
    }

    assert.equal(returned, input)
    assert.equal(output, Array.new(10, 20, 30))
    assert.equal(indexes, Array.new(0, 1, 2))
  }
}

test.group('std::array::Array.append') do (g) {
  g.test('Appending all the values of one Array to another Array') {
    let first = Array.new(10, 20, 30)
    let second = Array.new(40, 50, 60)

    assert.equal(first.append(second), Array.new(10, 20, 30, 40, 50, 60))
    assert.equal(first, Array.new(10, 20, 30, 40, 50, 60))
  }
}

test.group('std::array::Array.length') do (g) {
  g.test('Obtaining the number of values in an Array') {
    assert.equal(Array.new.length, 0)
    assert.equal(Array.new(10).length, 1)
    assert.equal(Array.new(10, 20, 30).length, 3)
  }
}

test.group('std::array::Array.empty?') do (g) {
  g.test('Checking if an Array is empty or not') {
    assert.true(Array.new.empty?)
    assert.false(Array.new(10).empty?)
  }
}

test.group('std::array::Array.Array.new') do (g) {
  g.test('Returning the value of an existing index') {
    let numbers = Array.new(10, 20, 30)

    assert.equal(numbers[0], 10)
    assert.equal(numbers[1], 20)
    assert.equal(numbers[2], 30)
  }

  g.test('Returning the value of a non-existing index') {
    let numbers = Array.new(10)

    assert.equal(numbers[1], Nil)
    assert.equal(numbers[2], Nil)
  }

  g.test('Returning the value of an existing negative index') {
    let numbers = Array.new(10, 20, 30)

    assert.equal(numbers[-1], 30)
    assert.equal(numbers[-2], 20)
    assert.equal(numbers[-3], 10)
    assert.equal(numbers[-4], 30)
    assert.equal(numbers[-5], 20)
    assert.equal(numbers[-6], 10)
  }
}

test.group('std::array::Array.[]=') do (g) {
  g.test('Setting the value of non-existing index') {
    let numbers = Array.new

    assert.equal(numbers[0] = 1, 1)
    assert.equal(numbers, Array.new(1))
  }

  g.test('Setting the value of an out-of-bounds index') {
    let numbers = Array.new

    assert.equal(numbers[2] = 1, 1)
    assert.equal(numbers[0], Nil)
    assert.equal(numbers[1], Nil)
    assert.equal(numbers[2], 1)
  }

  g.test('Setting the value of a negative index using an empty Array') {
    let numbers = Array.new

    assert.equal(numbers[-1] = 1, 1)
    assert.equal(numbers, Array.new(1))
  }

  g.test('Setting the value of a negative index using a non-empty Array') {
    let numbers = Array.new(10, 20, 30)

    numbers[-1] = 60
    numbers[-2] = 50

    assert.equal(numbers, Array.new(10, 50, 60))
  }
}

test.group('std::array::Array.to_array') do (g) {
  g.test('Converting an Array to another Array') {
    let numbers = Array.new(10, 20, 30)

    assert.equal(numbers, Array.new(10, 20, 30))
  }
}

test.group('std::array::Array.==') do (g) {
  g.test('Comparing two equal Arrays') {
    assert.equal(Array.new(10, 20), Array.new(10, 20))
  }

  g.test('Comparing two Arrays with a different length') {
    assert.not_equal(Array.new(10), Array.new(10, 20))
  }

  g.test('Comparing two Arrays that are not equal but have the same length') {
    assert.not_equal(Array.new(10), Array.new(20))
  }
}

test.group('std::array::Array.iter') do (g) {
  g.test('Iterating over an Array') {
    let numbers = Array.new(10, 20, 30)
    let iter = numbers.iter

    assert.equal(iter.next, 10)
    assert.equal(iter.next, 20)
    assert.equal(iter.next, 30)
    assert.equal(iter.next, Nil)
  }
}

test.group('std::array::Array.reverse_iter') do (g) {
  g.test('Iterating over an Array in reverse order') {
    let numbers = Array.new(10, 20, 30)
    let iter = numbers.reverse_iter

    assert.equal(iter.next, 30)
    assert.equal(iter.next, 20)
    assert.equal(iter.next, 10)
    assert.equal(iter.next, Nil)
  }
}

test.group('std::array::Array.contains?') do (g) {
  g.test('Checking if an Array contains a given value') {
    assert.false(Array.new.contains?(10))
    assert.false(Array.new(10).contains?(20))
    assert.true(Array.new(10, 20).contains?(10))
  }
}

test.group('std::array::Array.join') do (g) {
  g.test('Joining values in an empty Array') {
    assert.equal(Array.new.join(','), '')
  }

  g.test('Joining a single value') {
    assert.equal(Array.new(10).join(','), '10')
  }

  g.test('Joining multiple values') {
    assert.equal(Array.new(10, 20, 30).join(','), '10,20,30')
  }

  g.test('Joining multiple values using multiple characters as the separator') {
    assert.equal(Array.new(10, 20, 30).join('::'), '10::20::30')
  }

  g.test('Joining multiple values using an empty separator') {
    assert.equal(Array.new(10, 20, 30).join(''), '102030')
  }
}
